
----------------

# Repository layout
* [simulation](simulation/) is the core directory. It contains scripts to setup the test environnement, with a fake AP running FreeRadius and a fake client using wpa_supplicant. More information are available inside the directory.
* [spy](spy/) contains the sources to build the spy process.
* [trace_interpretation](trace_interpretation/) contains scripts related to parsing and automatic interpretaion of the collected samples.

# Attack on FreeRadius 

## Core idea

This cache attack targets EAP-pwd, which is Dragonfly variant implemented in FreeRadius. Our attack exploits password / control flow dependency in the implementation. This dependency occurs in the hunting and pecking procedure used to convert the password into an elliptic curve point. 
Due to the try-and-increment nature of this procedure, attackers can guess at which iteration the password has been successfuly converted into a point, thereby reducing the number of possible password.

Since the conversion depends not only on the password, but also on both parties identities and a random token, establishing multiple sessions allows attackers to acquire new traces and to tremendously reduce the size of the dictionary.

## Threat model

To exploit it, the attackers need to be able to monitor the CPU cache, using a classical Flush+Reload attack for instance. To do so, we assume the attackers are able to deploy a spy process, with no specific priviledges other than being able to read the OpenSSL shared library (which is a default permissions).

This spy process is assumed to run in background in order to record the CPU cache access to some specific functions.



## To Start
* run "./simulation/setup.sh -i" to build the test environment (this might take a while)
* run "sudo ./simulation/poc.sh -n 14 -d -p res_traces/<an existing directory>" in order to interpret the generated traces.
* run "sudo ./simulation/poc.sh -n 14 -s res_traces/<a new directory>" in order to generate new traces.


## More details can be found in README inside the simulation directory.
