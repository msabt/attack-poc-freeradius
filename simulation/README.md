# Simulation environment

This directory contains all the material to test the PoC by running both the client and server.

The simulation can be summed-up in two scripts:

## Setup script

Running `setup.sh` will compile both FreeRadius (with a local version of OpenSSL) and wpa_supplicant (that offers a handy client).
All the sources are available in [build_aux](build_aux/) and will be installed in [local_install](local_install/). The script also compiles the spy process, an unpriviledged process used to leverage the attack.

```
Usage: setup.sh -i|-u|-c
	-i: install (locally)
	-u: uninstall
	-c: clean build directories
```

Note that we added a few instructions in wpa_supplicant source code in order to easily access debug information. Namely, we print the identities, the random token and the iteration at which the password is derived. This is only for debug purposes (evaluate the success rate for our attack), and does not impact the attack process.

## Trace measurement and attack

Running `poc.sh` allows to the following
* setup the environment, 
* run FreeRadius and the client,
* establish some connections between the client and the AP, and measure the traces using the spy process,
* parse the traces to guess the number of round(s) needed to derive the password in a specific exchange,
* in debug mode, the real number of round is saved, allowing to evaluate the success rate of the attack.

```
Usage: ./poc.sh [-n NB_CONN] [-d] -a PASSWD|-s PASSWD|-p TRACES_DIR
	-a: simulate the attack on PASSWD and parse the resulted trace to guess the result. Same as -sp.
	-s: simulate the attack on PASSWD (i.e.: collect traces).
	-p: parse the traces located in TRACES_DIR. This directory should contain a subdirectory for monitored connections. Each subdirectory must contain two files: 
		* trace: contains the trace
		* debug: contains additionnal information such as the token, the identties and the real number of rounds.
	-d: run in debug mode. Display additional information (namely during the parsing).
	-n: perform the action for NB_CONN different connections. With -s, it starts NB_CONN, each one sing a different token. With -p, it only use the first NB_CONN traces. The default value is 15.
```

The outupt traces will be stored in a directory named `res_traces`, with a subdirectory for each tested password.

### Example

For instance, the following command will acquire traces for the password `super_strong_password`, repeating the connexion 14 times, resulting in 14 traces:
```bash
sudo ./poc.sh -n 14 -s super_strong_password
```
The results are interpreted using the following command:
```bash
sudo ./poc.sh -n 14 -p res_traces/super_strong_password
```

We achieve can combine these two commands with
```bash
sudo ./poc.sh -n 14 -a super_strong_password
```

## Disclaimer

Note that addresses to monitor are hardcoded in `poc.sh`. Hence, building on a different system, or with different option (optimizations, ...) may yield different results (it is only a PoC that was run on Fedora).