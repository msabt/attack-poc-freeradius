#!/bin/bash

set -e
source config.sh

install_sexpect() {
    cd ${SEXPECT_BUILD_DIR}
    make
    mkdir -p ${LOCAL_INST_DIR}/bin
    cp sexpect ${LOCAL_INST_DIR}/bin/
    cd ${SIMULATION_DIR}
}

clean_sexpect() {
    make -C ${SEXPECT_BUILD_DIR}  clean
}

install_openssl() {
	cd ${OPENSSL_BUILD_DIR}
	./config --debug --prefix=${LOCAL_INST_DIR} --openssldir=${LOCAL_INST_DIR}/ssl
	make -j
	make install
	cd ${SIMULATION_DIR}
}

clean_openssl() {
	make -C ${OPENSSL_BUILD_DIR} clean
	make -C ${OPENSSL_BUILD_DIR} distclean
}

install_freeradius() {
    cd ${FREERADIUS_BUILD_DIR}
    ./configure --prefix=${LOCAL_INST_DIR} --datadir=${LOCAL_INST_DIR} --with-talloc-lib_dir=/usr/lib64/  --with-openssl-include-dir=${LOCAL_INST_DIR}/include --with-openssl-lib-dir=${LOCAL_INST_DIR}/lib
    make -j 
    make install
    cd ${SIMULATION_DIR}
}

clean_freeradius() {
    make -C ${FREERADIUS_BUILD_DIR} clean
}

install_client() {
	cd ${WPA_SUPPLICANT_BUILD_DIR}
	[ -e .config ] || cp defconfig .config
	make -j eapol_test
	cp eapol_test ${LOCAL_INST_DIR}/bin
	cd ${SIMULATION_DIR}
}

clean_client() {
	make -C ${WPA_SUPPLICANT_BUILD_DIR} clean
}

build_spy() {
    cd ${ROOT_DIR}/spy
    [[ -f spy ]] || make
    cd ${SIMULATION_DIR}
}

clean_spy() {
    cd ${ROOT_DIR}/spy
    make clean
    cd ${SIMULATION_DIR}
}

uninstall() {
    rm -rf ${LOCAL_INST_DIR}/{bin,doc,include,lib,logs,sbin,share,ssl,var}
}


while getopts "iuc" opt; do
    case "$opt" in
    i)
        install_sexpect
        install_openssl
        install_freeradius
        install_client
        build_spy
        ;;
    u)  
        uninstall
        ;;
    c)  
        clean_sexpect
        clean_openssl
        clean_freeradius
        clean_client
        clean_spy
        ;;
    *)
        echo "Usage: $0 -i|-u|-c"
        echo -e "\t-i: locally install OpenSSL, FreeRadius, a Radius client (eapol_test from wpa_supplicant) ans sexpect (used in the PoC)"
        echo -e "\t-u: uninstall OpenSSL, FreeRadius, a Radius client (eapol_test from wpa_supplicant) and sexpect"
        echo -e "\t-c: clean build directories"
    esac
done
