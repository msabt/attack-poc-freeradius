# On quitting, we stop all processes
trap terminate SIGINT

# Some directories and files locations
SIMULATION_DIR=$(pwd)

# Building directories
BUILD_DIR="${SIMULATION_DIR}/build_aux"
FREERADIUS_BUILD_DIR="${BUILD_DIR}/freeradius-server-3.0.21"
OPENSSL_BUILD_DIR="${BUILD_DIR}/openssl-1.1.1g"
WPA_SUPPLICANT_BUILD_DIR="${BUILD_DIR}/wpa_supplicant-2.9/wpa_supplicant"
SEXPECT_BUILD_DIR="${BUILD_DIR}/sexpect"

LOCAL_INST_DIR="${SIMULATION_DIR}/local_install"

SEXPECT="${LOCAL_INST_DIR}/bin/sexpect"

TMP_DIR="${SIMULATION_DIR}/tmp"
SPY_SOCK="${TMP_DIR}/spy.sock"
RADIUS_SOCK="${TMP_DIR}/freeradius.sock"

TRACE_DIR="${SIMULATION_DIR}/res_traces"

# Path to binary
ROOT_DIR=$(cd .. && pwd && cd $SIMULATION_DIR)
SPY_BIN=${ROOT_DIR}/spy/spy
RADIUSD=${LOCAL_INST_DIR}/sbin/radiusd
EAPOL_TEST=${LOCAL_INST_DIR}/bin/eapol_test
PARSER=${ROOT_DIR}/trace_interpretation/parsing.py

# Path to configuration files
HOSTAP_CONF=${LOCAL_INST_DIR}/etc/eap-pwd.conf
RADIUS_EAP_CONF=${LOCAL_INST_DIR}/etc/raddb/mods_enable/eap
RADIUS_USER_CONF=${LOCAL_INST_DIR}/etc/raddb/mods-config/files/authorize


# Kill spawned process and remove temporary files
clean_up () {
	pkill sexpect
	pkill radiusd
    rm -rf ${TMP_DIR} 
}


# Properly exit
terminate () {
	clean_up
	
	[[ -d ${TRACE_DIR} ]] && chmod -R o+rw ${TRACE_DIR}
	[[ -e ${SIMULATION_DIR}/ground_truth ]] && chmod o+rw ${SIMULATION_DIR}/ground_truth
	[[ -e ${SIMULATION_DIR}/prediction ]] && chmod o+rw ${SIMULATION_DIR}/prediction
}