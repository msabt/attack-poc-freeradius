from sys import argv, exit
import re

# Label used to distinguish the timer
label_clock = "clock"
# Label used to distinguish the rigth iteration
label_found = "found"

def parse_one_trace(trace):
    n_rounds = 1

    # Remove leading found_label (not relevant)
    while label_found in trace[0]:
        trace = trace[1:]
 
    for line in trace[1:]:
        # Skip blank lines to avoid errors
        if line == "" or "=" in line:
            continue
        (event, pause, _) = line.split()
        pause = int(pause)

        if event == label_clock:
            n_rounds += 1
        elif event == label_found:
            break
                
    return n_rounds


def get_debug_infos(filename):
    with open(filename) as fp:
        infos = fp.readlines()
    token = infos[0].split()[2]
    peer_id = infos[1].split()[2]
    server_id = infos[2].split()[2]
    n_iter = int(infos[3].split()[3])
    
    return token, peer_id, server_id, n_iter


if len(argv) < 2:
    print("Usage: {} [-d] TRACE_FOLDER [TRACE_FOLDER ...]".format(argv[0]))
    exit(-1)

if argv[1] == "-d" or argv[3] == "-d":
    debug = True
    args = argv[2:]
else:
    debug = False
    args = argv[1:]

out = []
nb_error_without_warning=0
for trace_dir in args:
    if debug:
        print(trace_dir + ": ")
    warning = False
    # Recover debug infos
    token, server_id, peer_id, real_iter = get_debug_infos(trace_dir + "/debug")

    # Read trace
    with open(trace_dir + "/trace") as fp:
        trace = fp.read()
        trace = trace.split('\n')

    guess_iter = parse_one_trace(trace)

    if debug:
        print("\t* Token:", token)
        print("\t* User ID:", peer_id)
        print("\t* Server ID:", server_id)
        print("\t* Nb iteration:", guess_iter) 
    
    if guess_iter == real_iter -1:
        if debug:
                print("\t[WARNING] Missed one iteration ! Expected", real_iter)
    elif guess_iter != real_iter:
        if debug:
            print("\t[DEBUG] Wrong result ! Expected", real_iter)
        nb_error_without_warning += 1


    # Only keep the result if we are quite sure
    if debug:
        # Debug files, used to evaluate the performance of our parsing
        with open("prediction", "a") as fp_pred:
            fp_pred.write(str(guess_iter) + "\n")
        with open("ground_truth", "a") as fp_truth:
            fp_truth.write(str(real_iter) + "\n")
    out.append((guess_iter, token, peer_id, server_id))


if debug:
    if nb_error_without_warning > 0:
        print("[DEBUG] {} errors without warnings".format(nb_error_without_warning))
for trace in sorted(out, reverse=True):
    print("{},{},{},{}".format(trace[1], trace[2], trace[3], trace[0]), end=' ')
