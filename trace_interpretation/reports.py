#!/usr/bin/env python

from sklearn.metrics import classification_report
import pandas as pd
from sys import argv

try:
	prediction_filename = argv[1]
	ground_truth_filename = argv[2]
except Exception as e:
	print("Usage: {} PREDICTION_FILE TRUTH_FILE".format(argv[0]))
	raise e

f_pred = pd.read_csv(prediction_filename, header=None)
f_truth = pd.read_csv(ground_truth_filename, header=None)
y_pred = f_pred.values[::]
y_truth = f_truth.values[::]
print(classification_report(y_truth, y_pred))
print()
